package org.springframework.springbootnettywebsokcetstarter.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface SocketModule {

    /**
     * @fields socket 指令模块编码 默认可以为空
     * @author liuChuanqiang
     */
    String module() default "default";

}
