package org.springframework.springbootnettywebsokcetstarter.annotations;


import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface SocketCommand {

    /**
     * @fields socket:指令编码
     * @author liuChuanqiang
     */
    String command();
}
