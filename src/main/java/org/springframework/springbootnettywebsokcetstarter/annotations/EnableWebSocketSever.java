package org.springframework.springbootnettywebsokcetstarter.annotations;


import org.springframework.context.annotation.Import;
import org.springframework.springbootnettywebsokcetstarter.core.WebSocket;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(WebSocket.class)
@Documented
@Inherited
public @interface EnableWebSocketSever {
	Class value() ;
}
