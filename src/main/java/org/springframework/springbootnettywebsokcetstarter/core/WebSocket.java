package org.springframework.springbootnettywebsokcetstarter.core;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Log4JLoggerFactory;
import org.springframework.springbootnettywebsokcetstarter.filter.WssServerInitialzer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

/**
 * @version V1.0.0
 * @Title: WebSocket
 * @Package com.dreamac.imwebsocketstarter.core
 * @Description:  webSocket
 * @author: liuChuanqiang
 * @date: 2019/7/9 12:22
 */
@Component
public class WebSocket {

	private static  final InternalLogger log = Log4JLoggerFactory.getInstance(WebSocket.class);

	@Resource
	private WebSocketNettyConfig webSocketNettyConfig;

	@Resource
	private WssServerInitialzer wssServerInitialzer;

	private EventLoopGroup mainGroup;

	private EventLoopGroup subGroup;

	private ChannelFuture future;

	@PostConstruct
	public void webSocketStart()throws Exception{
		mainGroup = new NioEventLoopGroup(webSocketNettyConfig.getBossCount());
		subGroup = new NioEventLoopGroup(webSocketNettyConfig.getWorkerCount());
		ServerBootstrap server = new ServerBootstrap();
		server.group(mainGroup, subGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(wssServerInitialzer);
		//添加自定义初始化处理器
		future= server.bind(webSocketNettyConfig.getPort()).sync();
		log.info("web Socket start {} ", webSocketNettyConfig.getPort());
	}

	@PreDestroy
	public void webSocketStop()throws Exception{
		future.channel().closeFuture().sync();
		mainGroup.shutdownGracefully();
		subGroup.shutdownGracefully();
	}


}
