package org.springframework.springbootnettywebsokcetstarter.invoker;


/**
 * @version V1.0.0
 * @Title: Result
 * @Package com.deramac.imgamebarrel.vo
 * @Description: 返回结果封装类
 * @author: liuChuanqiang
 * @date: 2019/7/10 11:18
 */
public class Result<T> {

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getSuccess() {
		return isSuccess;
	}

	public void setSuccess(Boolean success) {
		isSuccess = success;
	}

	public String getSubCode() {
		return subCode;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	public String getSubMsg() {
		return subMsg;
	}

	public void setSubMsg(String subMsg) {
		this.subMsg = subMsg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	/**
	 * 返回结果 success error 成功或错误
	 */
	private String code;

	/**
	 * 判断是否成功
	 */
	private Boolean isSuccess;

	/**
	 * 错误码
	 */
	private String subCode;

	/**
	 * 错误的消息
	 */
	private String subMsg;

	/**
	 * data
	 */
	private T data;

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	/**
	 * 指令
	 */
	private String cmd;

	/**
	 * 成功构造
	 * @param t
	 * @return
	 */
	public static Result groupSend(Object t,String cmd){
		Result result = new Result();
		result.cmd = cmd;
		result.code = "SUCCESS";
		result.isSuccess = true;
		result.data = t;
		return result;
	}

	/**
	 * 成功构造
	 * @param t
	 * @return
	 */
	public static Result success(Object t){
		Result result = new Result();
		result.code = "SUCCESS";
		result.isSuccess = true;
		result.data = t;
		return result;
	}

	/**
	 * 失败构造
	 * @param subCode
	 * @param subMsg
	 * @return
	 */
	public static Result error(String subCode,String subMsg){
		Result result = new Result();
		result.code = "ERR";
		result.isSuccess = false;
		result.subCode = subCode;
		result.subMsg = subMsg;
		return result;
	}

}
