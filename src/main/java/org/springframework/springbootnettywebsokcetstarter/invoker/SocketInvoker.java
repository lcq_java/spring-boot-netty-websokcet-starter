package org.springframework.springbootnettywebsokcetstarter.invoker;

import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;


/**
 * 执行器
 * @author Administrator
 */
public class SocketInvoker {

    /**
     * 目标对象
     */
    private Object target;

    /**
     * 方法
     */
    private Method method;

    /**
     * @Title  valueOf
     * @Description 初始化socket执行器
     * @author liuChuanqiang
     * @updateAuthor liuChuanqiang
     * @Date 2019/5/27 16:13
     * @param object
     * @param method
     * @version v1.0.0
     * @exception
     * @throws
     * @return com.deramac.invokerstarted.core.SocketInvoker
     **/
    public static SocketInvoker valueOf(Object object, Method method) {
        SocketInvoker invoker = new SocketInvoker();
        invoker.setTarget(object);
        invoker.setMethod(method);
        return invoker;
    }

    /**
     * @Title  invoke
     * @Description 执行参数为map的执行器
     * @author liuChuanqiang
     * @updateAuthor liuChuanqiang
     * @Date 2019/5/27 16:14
     * @param map
     * @version v1.0.0
     * @exception
     * @throws
     * @return java.lang.Object
     **/
    public Object invoke(Map<String, Object> map) throws Throwable {
        Object obj = null;
        try {
            obj = method.invoke(target, map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Throwable targetException = e.getTargetException();
            throw targetException;
        }
        return obj;
    }

    /**
     * @Title  invoke
     * @Description 执行参数为 args 的执行器
     * @author liuChuanqiang
     * @updateAuthor liuChuanqiang
     * @Date 2019/5/27 16:14
     * @param args
     * @version v1.0.0
     * @exception
     * @throws
     * @return java.lang.Object
     **/
     public Object invoke(Object... args) throws Throwable {
        Object obj = null;
        try {
            obj = method.invoke(target, args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            throw targetException;
        }
        return obj;
    }
    /**
     * @Title  invoke
     * @Description 执行参数为 Object 的执行器
     * @author liuChuanqiang
     * @updateAuthor liuChuanqiang
     * @Date 2019/5/27 16:17
     * @param object
     * @version v1.0.0
     * @exception
     * @throws
     * @return java.lang.Object
     **/
    public Object invoke(Object object) throws Throwable {
        Object obj = null;
        try {
        	if (object instanceof JSONObject ){
        		JSONObject jsonObject = (JSONObject)object;
				return invoke(jsonObject);
			}
            obj = method.invoke(target, object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            throw targetException;
        }
        return obj;
    }
    /**
     * @Title  invoke
     * @Description 无参数
     * @author liuChuanqiang 
     * @updateAuthor liuChuanqiang
     * @Date 2019/7/10 12:21 
     * @param 
     * @version v1.0.0
     * @exception 
     * @throws   
     * @return java.lang.Object
     **/
    public Object invoke() throws Throwable {
        Object obj = null;
        try {
            obj = method.invoke(target);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            throw targetException;
        }
        return obj;
    }

    public Object invoke(JSONObject jsonObject) throws Throwable {
        Object obj = null;
        try {
			Class<?>[] parameterTypes = method.getParameterTypes();
			Object object = null;
			for (Class clazz : parameterTypes){
				object = JSONObject.toJavaObject(jsonObject, clazz);
				continue;
			}
			obj = method.invoke(target,object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            throw targetException;
        }
        return obj;
    }

    /**
     * @Title  setTarget
     * @Description setTarget 方法
     * @author liuChuanqiang
     * @updateAuthor liuChuanqiang
     * @Date 2019/5/27 16:15
     * @param target
     * @version v1.0.0
     * @exception
     * @throws
     * @return void
     **/
    public void setTarget(Object target) {
        this.target = target;
    }

    /**
     * @Title  setMethod
     * @Description setMethod 方法
     * @author liuChuanqiang
     * @updateAuthor liuChuanqiang
     * @Date 2019/5/27 16:15
     * @param method
     * @version v1.0.0
     * @exception
     * @throws
     * @return void
     **/
    public void setMethod(Method method) {
        this.method = method;
    }

}
